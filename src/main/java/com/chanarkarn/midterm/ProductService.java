/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.midterm;

import java.util.ArrayList;
import java.util.Collection;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author A_R_T
 */
public class ProductService implements Serializable {

    private static ArrayList<Product> ProductList = ProductList = new ArrayList<>();


    public static boolean addProduct(Product product) {
        ProductList.add(product);
        save();
        return true;
    }

    public static boolean Product(Product product) {
        ProductList.remove(product);
        save();
        return true;
    }

    public static void delAllProduct() {
        ProductList.remove(ProductList);
        save();

    }

    public static ArrayList<Product> getProduct() {
        return ProductList;
    }

    public static Product getProduct(int index) {
        return ProductList.get(index);
    }

    public static boolean updateProduct(int index, Product product) {
        ProductList.set(index, product);
        save();
        return true;
    }

    public static double getTotalPrice() {
        double sum = 0;
        for (Product product : ProductList) {
            sum += (product.getAmount() * product.getPrice());
        }
        return sum;
    }

    public static int getAmount() {
        int amount = 0;
        for (Product product : ProductList) {
            amount += product.getAmount();
        }
        return amount;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            file = new File("Kik.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ProductList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            file = new File("Kik.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ProductList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static boolean delProduct(int index) {
        ProductList.remove(index);
        return true;
    }

    public static boolean editGoods(int index,Product goods){
        ProductList.set(index, goods);
        save();
        return true;
    }

}
