/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.midterm;

import java.io.Serializable;

/**
 *
 * @author A_R_T
 */
public class Product implements Serializable{
    private String ID;
    private String name;
    private String brand;
    private double price;
    private int amount;

    Product(String id,String name,String brand ,double price,int amount){
        this.ID = ID;
        this.name = name;
        this.brand =brand;
        this.price = price;
        this.amount = amount;
    }

   public String getID() {
        return ID;
    }
    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return ID+"                             "+name+"                           "+brand+"                             "+price+"                            "+amount;
    }
    
}
